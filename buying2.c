#include <stdio.h>
#include <math.h>
int main()
{
	int t,n,x,value[100],add = 0,i;
	scanf("%d",&t);
	while(t>0)
	{
		scanf("%d %d", &n, &x);
		for(i=0;i<n;i++)
		{
			scanf("%d",&value[i]);
			add = add+ value[i];
		}
		if( add % x == 0 || add > x)
		{
			printf("%d\n", add/x);
		}
		else
		{
			printf("-1\n");
		}
		t--;
	}
	return 0;
}