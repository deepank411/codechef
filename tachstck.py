N, D = map(int,raw_input().split())
L = [0] * N  ## length of chopsticks
j = 0  ## no. of sticks used
count = 0  ## no. of useful pairs
for i in range(N):
    L[i] = int(raw_input())
L.sort() 
while j < (N-1):
    if L[j+1]-L[j] <= D:
        count += 1
        j += 2
    else:
        j += 1
print count
