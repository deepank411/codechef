import gc; gc.disable()
import sys
N = int(sys.stdin.readline())
radius = sys.stdin.readline().split(' ')
radius = [int(x) for x in radius]
M = int(sys.stdin.readline())
total = 0
for i in range(1,M+1):
    x1,y1,x2,y2 = sys.stdin.readline().split()
    x1,y1,x2,y2 = int(x1),int(y1),int(x2),int(y2)
    first = ((x1**2)+(y1**2))
    second = ((x2**2)+(y2**2))
    for i in radius:
        rad = i**2
        if (first < rad and second > rad) or (first > rad and second < rad):
            total = total + 1
print total
