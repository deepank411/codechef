T = int(raw_input())
for _ in range(T):
    G = int(raw_input())
    for _ in range(G):
        I, N, Q = map(int,raw_input.split())
        total = 0
        arr = [I] * N
        for i in len(arr):
            if (arr[i] == 1):
                arr[i] = 0
            elif (arr[i] == 0):
                arr[i] = 1
