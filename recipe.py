T = int(raw_input())
for _ in range(T):
    inp = map(int,raw_input().split())
    N = inp[0]
    def gcd(a,b):
        while b:
           a,b = b, a%b
        return a

    def gcd_list(list):
        return reduce(gcd,list)
    x =  gcd_list(inp[1:])
    for i in inp[1:]:
        print i/x,
