T = int(raw_input())
for _ in range(T):
    N, C = raw_input().split()
    N, C = int(N), int(C)
    A = raw_input().split(' ')
    A = [int(x) for x in A]
    if (sum(A) <= C):
        print 'Yes'
    else:
        print 'No'
