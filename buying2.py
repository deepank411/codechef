T = int(raw_input())
while T > 0:
    N,X = raw_input().split()
    N,X = int(N), int(X)
    value = raw_input().split(' ')
    value = [int(x) for x in value]
    add = sum(value)
    if (add % X == 0 and add > X):
        print (add/X)
    else:
        print '-1'
    T = T - 1
