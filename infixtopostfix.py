t = int(raw_input())
for _ in range(t):
    i = raw_input() ## infix expression
    p = "" ## postfix expression
    o = [] ## operators list
    for j in i:
        if (j=='('):
            continue
        elif(j>='a' and j<='z'):
            p = p + j
        elif(j==')'):
            p = p + o.pop()
        else:
            o.append(j)
    print p
