#include <stdio.h>
#include <math.h>
 
int main()
{ 
	long n,radius[1000000],m,i,x1,x2,y1,y2,first,second,rad,total=0;
	scanf("%ld", &n);
	for(i=0;i<n;i++)
	{
		scanf("%ld ",&radius[i]);
	}
	scanf("%ld",&m);
	for(i=0;i<m;i++)
	{
		scanf("%ld %ld %ld %ld",&x1,&y1,&x2,&y2);
		first = (x1*x1)+(y1*y1);
		second = (x2*x2)+(y2*y2);
		for(i=0;i<n;i++)
		{
			rad = (radius[i]*radius[i]);
			if ((first<rad && second>rad) || (first>rad && second<rad))
			{
				total++;
			}
		}
	}
	printf("%ld",total);
	return 0;
}